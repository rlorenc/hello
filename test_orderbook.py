from copy import deepcopy

adds = []
modifys = []
cancels = []
old_bids = []
"""
for item in snapshot:
    if item["ordertype"] == "Buy":
	    		new_bids.append({
	    			"price": item["price"],
	    			"amount": item["quantity"]
	    		})
	    	elif item["ordertype"] == "Ask":
	    		new_asks.append({
	    			"price": item["price"],
	    			"amount": item["quantity"]
	    		})
            else:
                raise Exception("Invalid coinagy ordertype: {}".format(item["ordertype"]))
"""
new_bids = []
new_bids.append({"price": 1.2,"amount": 5})
new_bids.append({"price": 4.564,"amount": 5})
new_bids.append({"price": 6.55,"amount": 5})
new_bids.append({"price": 8.123,"amount": 10})

# if this is a first snapshot, we need to populate the full book
if len(old_bids) == 0 and len(new_bids) > 0:
    for bid in new_bids:
        adds.append(bid)
    old_bids = deepcopy(new_bids)
    print ("New bids:")
    print (new_bids)
    print ("Adds:")
    print (adds)
    print ("Old bids:")
    print (old_bids)

# if previous state of the book exists, proceed with the diffrence calc
# for bids
#new_bids.append({"price": 1.01,"amount": 15})
#new_bids[0]["amount"] = 44
#new_bids[3]["amount"] = 844
#new_bids.append({"price": 1.02,"amount": 15})
#new_bids.append({"price": 1.03,"amount": 15})
#new_bids.append({"price": 1.04,"amount": 15})
del new_bids[2]

new_bids.sort(key=lambda e: e["price"])
print ("New bids:")
print (new_bids)

counter1 = 0
counter2 = 0
counter3 = max(len(new_bids),len(old_bids))
counter4 = len(new_bids)
counter5 = len(old_bids)
adds = []
modifys = []
cancels = []
print (new_bids[0]["price"])
print (len(old_bids))
print (len(new_bids))

while counter3 > 0:
    if new_bids[counter1]["price"] < old_bids[counter2]["price"]:
        adds.append(new_bids[counter1])
        counter1 += 1
        counter1 = min(counter1,counter4)
        counter3 -= 1
        print ("Counter1 First:", counter1)
        print ("Counter2 First:", counter2)
        print ("Counter3 First:", counter3)
        continue
    else:
        if new_bids[counter1]["price"] == old_bids[counter2]["price"]:
            #and new_bids[counter1]["amount"] != old_bids[counter2]["amount"]:
            modifys.append(new_bids[counter1])
            counter1 += 1
            counter1 = min(counter1,counter4)
            counter2 += 1
            counter2 = min(counter2,counter5)
            counter3 -= 1
            print ("Counter1 Second:", counter1)
            print ("Counter2 Second:", counter2)
            print ("Counter3 Second:", counter3)
            continue
        else:
            if new_bids[counter1]["price"] > old_bids[counter2]["price"]:
                cancels.append(old_bids[counter2])
                counter2 += 1
                counter2 = min(counter2,counter5)
                counter3 -= 1

    #counter3 -= 1
    print ("Counter1 End:", counter1)
    print ("Counter2 End:", counter2)
    print ("Counter3 End:", counter3)

print ("Adds:")
print (adds)
print ("Replaces:")
print (modifys)
print ("Cancels:")
print (cancels)
		#old_bids = deepcopy(new_bids)
#print ("New Adds:")
#print (adds)
#while counter1 <= new_bids.count:
#    if new_bids[coutner1].price < old_bids[counter2].price
#        adds.append(new_bids[counter1])
#        counter1 += 1
#        continue
#    elif new_bids[counter1].price == old_bids[counter2].price
#        if new_bids[counter1].amount != old_bids[counter2].amount
#            modifys.append(new_bids[coutner1])
#        coutner1 += 1
#        counter2 += 1
#        continue
#    elif new_bids[coutner1].price > old_bids[counter2].price
#        cancels.append(old_bids[counter2])
#        counter2 += 1
#        continue

# now loop over the existing bid's...
# where there is a matching entry in new_bids,
# cancels should be send before modifications and before new order
